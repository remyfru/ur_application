#include <ros/ros.h>
#include <std_msgs/Float64MultiArray.h>

int main(int argc, char* argv[]) {
    ros::init(argc, argv, "ur_application");

    ros::NodeHandle node;

    auto left_pos_pub = node.advertise<std_msgs::Float64MultiArray>(
        "/left_arm_controller/command", 1);

    std_msgs::Float64MultiArray left_cmd;
    left_cmd.data.resize(6, 0.);

    const double sample_time = 0.01;
    const double velocity = 0.5;
    ros::Rate rate(1. / sample_time);

    double time = 0.;
    while (ros::ok()) {
        for (auto& p : left_cmd.data) {
            p += velocity * sample_time;
        }

        left_pos_pub.publish(left_cmd);

        ros::spinOnce();

        rate.sleep();

        time += sample_time;
        if (time > 2.) {
            break;
        }
    }
}
